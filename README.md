# mars

https://mars.nasa.gov/

http://www.mars.asu.edu/data/

https://www.google.com/mars/

https://fmaps.gitlab.io/mars/


Mars_MGS_MOLA_DEM_mosaic_global_463m

Data:

This digital elevation model (DEM) is based on data from the Mars Orbiter Laser Altimeter (MOLA; Smith et al., 2001), an instrument on NASA’s Mars Global Surveyor (MGS) spacecraft (Albee et al., 2001). The MOLA DEM represents more than 600 million measurements gathered between 1999 and 2001, adjusted for consistency (Neumann et al., 2001; Neumann, Smith & Zuber, 2003) and converted to planetary radii. These have been converted to elevations above the areoid as determined from a Martian gravity field solution GMM-2B (Lemoine et al., 2001), truncated to degree and order 50, and oriented according to current standards. The average accuracy of each point is originally ~100 meters in horizontal position and ~1 meter in radius (Neumann et al., 2001). However, the total elevation uncertainty is at least ±3 m due to the global error in the areoid (±1.8 meters; Neumann et al., 2001) and regional uncertainties in its shape (Neumann, 2002). Pixel resolution is 463 meters per pixel (m).

https://astrogeology.usgs.gov/search/map/Mars/GlobalSurveyor/MOLA/Mars_MGS_MOLA_DEM_mosaic_global_463m
